New banner content

- [ ] Create new content-type "banner" in contentful
- [ ] Put banners on the screen if we get them

Fix event data
- [ ] copy database from prod and make local
- [ ] PUll event data from psql
- [ ] parse it similarly as we did from firebase
- [ ] pull out firebase stuff


Evelin's wish-list

- [ ] new members event page
- [ ] Calendar view

# Future

- [x] Make navbar automatic
  - [x] Turn into hamburger based on content, not hard-coded widths
  - [x] Do it better, using media-queries from JavaScript
- [ ] Events
  - [ ] filter to 'social' only
  - [ ] enable click-on-event
- [ ] Get the whole thing behind Fastly.

## Post-membership:

- [ ] Search feature
- [ ] Get an email when things change:
- [ ] An updates section:
- [ ] Use contentful's version history.

# Content

- [ ] Ensure that it's two or three clicks to get to anything
  - [ ] Sponsor feedback -- way too many clicks to get to a web survey form
- [ ] New "Member Incident Incident" survey form https://app.contentful.com/spaces/z60c05cc1iui/entries/gNxu4yDe7KoKGco2GCmSI
- [ ] Need to be published. Take off "Gabriella". Verify all the links are good.
- [ ] Go through all the content and make sure we don't refer to Meetup Pages anywhere. Only content in the sexpositiveportland.org site.
- [ ] Rename "Care and Confidentiality Agreement" to "Play Safe agreement" -- new link from Google Docs
- [ ] Go through any un-published content and remove it from Contentful
- [ ] How to get a message from SPP Members: has graphics (from Meetup.com)
- [ ] Clean up Volunteer Credits
- [ ] Levelling up checklist: make this a much better checklist UI
- [ ] Changing the Positively Kinky levelling system (TBD)
- [ ] Stars Worksheet:
  - Current one is ugly
  - Will break out STI information into its own thing
  - Get two-page version onto the site (google docs)
- [ ] Liability waiver: don't publish this. delete it from contentful
- [ ] "Resolving Member Complaints" has a link for "Fill out this complaint form" which should point to the survey document
- [ ] New document: Sex Positive Portland Therapist/Educational resources
- [ ] Meetup "What is SPP about" should be in our home-page
- [ ] Link to dues status (under discussion in Meetup home page)
