export interface PathType {
  path: string
  content: string
}

export interface PageType {
  [key: string]: PathType
}
