export interface ContentType {
  content: string
  path?: string
  tags?: string[]
}

interface CategoryFieldType {
  title: string
}
interface CategoryType {
  fields: CategoryFieldType
}

interface ContentfulFileType {
  url: string
}

interface ImageFieldType {
  title: string
  file: ContentfulFileType
}

interface ImageType {
  fields: ImageFieldType
}

export interface HomepageField {
  body: string
  secondaryBody: string
  category: CategoryType[]
  featuredImage: ImageType
  order: number
  title: string
}

export interface HomepageContentType {
  category: string
  title: string
  body: string
  secondaryBody: string
  order: number
  image: string
}

export interface DocumentType {
  [key: string]: ContentType
}
interface BlockType {
  body: string
  imageUrl: string
  imageDimentions: string
  slug: string
  sortOrder: number
}
