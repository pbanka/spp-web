export interface LinkType {
  tags: string[]
  order: Number
  slug: string
  uri: string
  label: string
}

export interface ContentType {
  links: LinkType[]
  content: ContentType
}

export interface CookieType {
  spp?: string
}
