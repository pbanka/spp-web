export interface QueryType {
  tag?: string | string[]
  title?: string | string[]
}
