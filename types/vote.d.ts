export interface Candidate {
  id: number
  meetup_member_id: number
  name: string
  photo_url: string
}

export type CandidateInput = {
  id: number
}

export interface Election {
  slug: string
  title: string
  id: number
  opened_at: null | string
  closed_at: null | string
  candidates: Candidate[]
  number_of_seats: number
}

export enum ElectionStatus {
  NotOpen = "notOpen",
  Open = "open",
  Closed = "closed",
}

export interface Vote {
  id: number
  candidate_id: number
}

export interface Ballot {
  election_slug: string
  id: string
  votes: Vote[]
}

export interface VoteInput {
  candidate_id: number
}

export type BallotInput = {
  election_slug: string,
  votes: VoteInput[]
}
