import { Ballot, Election, VoteDataType } from "./vote"
import { QueryType } from "./query"
import { HomepageContentType } from "./document"

export interface RequestType {
  originalUrl: string;
  cookies: any
}

export interface ResponseType {
  writeHead: (code: number, headers: any) => void;
  end: () => void;
  cookie: (string, string, any) => void
  clearCookie: (string, string?, any?) => void
}

export interface NextJsContext {
  req: RequestType;
  query: QueryType;
  res: any;
}

interface AppType {
  query: QueryType
  content: any
  events: []
}

interface BlogContent {
  pages: any // TODO
  documents: DocumentType
  tagNames: string[]
  documentNames: string[]
}

export interface DocumentMap {
  [key: string]: DocumentType
}

export interface SystemProps {
  app: AppType
  pages: any // TODO
  cookies: CookieType
  documents: any
  banners: BannerType[]
  // documents: DocumentMap
  tagNames: string[]
  posts: HomepageContentType[]
  documentNames: string[]
  voting: VoteDataType
  blocks: BlockType[]
  jwt: string,
  ballots: Ballot[],
  elections: Election[],
}
