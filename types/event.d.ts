export default interface Event {
  description: string
  id: number
  isPublic: boolean
  signupUrl: string
  startAt: Date
  title: string
}
