# Provisioning A Server For Deployment

The server will need to have `git` installed first.

## Install

- Clone the repository:

  ```
  git clone https://pbanka@bitbucket.org/pbanka/spp-web.git
  ```

- cd to the repository: `cd spp-web`

- Make sure you have node `8.x` or `9.x` installed
  - on ubuntu linux:
  ```sh
   curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh
   bash nodesource_setup.sh
   apt-get install nodejs
   ```
- Install the dependencies: `npm install`
- Build the app: `npm run build`
- Create the initial `.env` file: `cp .env.default .env`
  - This file contains secrets for the database access, Meetup, etc. Get these from Slack.

## Set up the server

- Install MongoDB:
  - On macOS:
    ```
    brew install mongodb && brew services start mongodb
    ```

- Install PM2: `npm install -g pm2`

- run the server: `npm run start`

- list server processes: `pm2 list`

- restart server processes `pm2 restart index`

### Set up the server to start on reboot

- `pm2 startup systemd`
- copy-and-paste the last line (for example:)
```
sudo env PATH=$PATH:/usr/bin /usr/lib/node_modules/pm2/bin/pm2 startup systemd -u pbanka --hp /home/pbanka
```

### Configure NGINX
modify `/etc/nginx/sites-available/default`
```
location / {
        proxy_pass http://localhost:3000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
```

- check the syntax of the config file: `sudo nginx -t`
- restart the nginx process with the new configs: `sudo systemctl restart nginx`

## Start the process to download L1 events

- run it manually to make sure it works: `cd download;npm install;node download.js`

- modify a cron job
  - run `crontab -e`
  - add the following line to the end:
  ```
  0 5 * * 1 'node download.js' /home/pbanka/spp-web/utils0 5 * * 1 'node download.js' /home/pbanka/spp-web/utils
  ```
