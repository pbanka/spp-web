# SPP Web Frontend

The SPP website service.

This service:

- Provides the framework around the website's pages
- Gets page content from Contentful and displays it
- Gets event data from Meetup and displays it

## Development Setup

Follow these directions to install the service for local development.

### Install Node and NPM

If npm is not installed already, it will need to be.

To check installation:

```
which npm
```

If the above command does not output anything, then you will need to
install npm by installing Node: https://nodejs.org/en/

### Clone Git Repo

- clone down the repository:
  ```
  git clone https://pbanka@bitbucket.org/pbanka/spp-web.git
  ```
- cd to the repository

### Create the .env file

Copy the `.env.default` file to `.env`:

```shell
cp .env.default .env
```

### Backing Services

#### Install MongoDB

MongoDB is not needed for most cases. If you don't need it, in your `.env` file,
make sure `MONGO_URI` is set to empty, or not set at all.

- On macOS:
  ```
  brew install mongodb && brew services start mongodb
  ```
- If the above command outputs `command not found`, you will need to
  install Homebrew first (Honebrew is for MacOS only).

#### Install PM2: `npm install -g pm2`

- run the server: `npm run start`
- list server processes: `pm2 list`
- restart server processes `pm2 restart index`

### Install

- Make sure you have node `8.x` or `9.x` installed
  - On Ubuntu Linux:
    ```sh
    curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh
    bash nodesource_setup.sh
    apt-get install nodejs
    ```
- Install the dependencies: `npm install`
- Build the app: `npm run build`
- Create the initial `.env` file: `cp .env.default .env`
  - This file contains secrets for the database access, Meetup, etc. Get these from Slack.

### SPP Core Service

The SPP website service depends on the SPP Core service. Normally, the frontend
can connect to the production (or staging when that's available). But some
situations may require testing against a local development version of the Core
service. Follow the instructions described for that service to start the service.

### Start The Service

```shell
npm run dev
```

You should now be able to visit http://localhost:3000/ from your web browser.

### Authorization for Local Development Using OAuth Flow

In order to test a logged-in user, the service will need to be configured to support it. Follow the
instructions in this section or the next section to set it up.

Ideally, a real OAuth consumer would be set up with Meetup for the purposes of local testing. Here's
how to do that:

1. Go to the Meetup OAuth Consumers page to create a consumer: https://secure.meetup.com/meetup_api/oauth_consumers/
2. Click the button to create a new consumer
3. Fill out the form.
4. For the "consumer name", enter "SPP Website - development"
5. For the "application website", enter `https://www.sexpositiveportland.org/`
6. For the redirect URL, enter `https://api.sexpositiveportland.org/spp-oauth-callback-dev`
7. For the question "What is your primary reason for using the Meetup API?", select "I want to use it for a personal project",
  and enter your name.
8. For the question "What is your primary use case for the Meetup API?", select "Building a tool for my Meetup group"
9. For the question "Full Description of Use Case", enter:
  """
  We would like to test our website as run in a local development environment. The website uses Meetup's OAuth 2 "Implicit Flow" to log the user into the website. We already have OAuth 2 credentials for the production use case. We just need separate credentials for a slightly different use case for testing the website for local development.
  """
7. For the explanation field, just say you're doing it for local testing

It takes at least one business day for Meetup to approve the request. Once approved, you will be issued
an OAuth `client_id` and `client_secret`. Paste the `client_id` into the value for the `MEETUP_OAUTH_CLIENT_ID`
environment variable defined in `.env`. The `client_secret` will not be used.

### Authorization for Local Development Using Impersonation

Alternatively, a user can be impersonated to simulate a logged-in status. This is an alternative to
the solution described in the section above.

Get a new JWT like this:

```shell
curl -i -XPOST -d 'grant_type=client_credentials&client_id=fake_1&client_secret={CLIENT_SECRET}' https://api.sexpositiveportland.org/oauth/token
```

For `{CLIENT_SECRET}`, get the secret value from the `config/config.secret.exs`, in the `backdoor_oauth_clients`
section of the Core codebase. There are two fake users described in the section.
The curl example above uses the `fake_1` user. Either one
will work, as long as `client_id` value reflects the chosen user.

This technique will also work against a locally-running SPP Core service.
Start up this service as described in the README for that project, and replace
`https://api.sexpositiveportland.org/` in the curl command with `http://localhost:4040/`

The curl command will return JSON that includes an `access_token` property, which is the JWT.

Pass the JWT when starting the frontend service:

```shell
JWT={JWT} npm run dev
```

Replace `{JWT}` with the JWT value.

Alternatively, you can add the JWT to the `.env` file.

## Running Service After a Code Update

Follow these instructions after changes are pulled in from upstream:

```shell
npm install
npm run dev
```

## Provisioning

To provision a server to deploy this service for the first time, follow the
instructions in guides/provisioning.md.

## Deploying

1. Ensure your desired changes are in the `master` branch.
2. SSH into the server as `pbanka`. Make sure you are in the `spp-web` directory (`cd ~/spp-web`)
3. Ensure you're on the correct branch: `git status`
4. Perform any cleanup if necessary: `git reset --hard`
5. Run `git pull` to pull in the changes from `master`.
6. Run `git reset --hard` to ensure no outstanding changes exist
7. Install packages: `npm install`
8. Create the build: `npm run build`
9. Restart the service: `pm2 restart index`
