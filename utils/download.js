const axios = require('axios')
const firebase = require('firebase')
const config = require('../config')

firebase.initializeApp(config.firebase)
const defaultDatabase = firebase.database()

const KEY = '25464e675ba3d960a12a6e537f0'
const GROUP_NAME = 'sexpositivepdx'
const url = `https://api.meetup.com/events?key=${KEY}&group_urlname=${GROUP_NAME}`

async function getEvents() {
  const output = await axios.get(url)
  const publicEvents = output.data.results
    .map(event => {
      const match = event.name
        .toLowerCase()
        .match(/(wine social|coffee social|public class|public dance)/)
      const publicType = match ? match[0] : 'private'
      return Object.assign(event, { publicType })
    })
    .filter(publicEvent => publicEvent.publicType !== 'private')
  publicEvents.forEach(event =>
    console.log(`${event.name} / ${event.publicType}\n\n${event.description}`)
  )
  await defaultDatabase.ref('events/').set(publicEvents)
}

getEvents().then(() => process.exit(0))
