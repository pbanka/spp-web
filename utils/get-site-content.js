import * as contentful from 'contentful'
import firebase from 'firebase'
import { parseCookies } from 'nookies'
const config = require('../config')

const client = contentful.createClient(config.contentful)

const getBlogContent = async query => {
  let serverData = { pages: {}, documents: {}, links: [] }
  try {
    const entries = await client.getEntries()
    entries.items.forEach(story => {
      const type = story.sys.contentType.sys.id + 's'
      const fields = story.fields
      if (['documents', 'pages'].includes(type)) {
        serverData[type][fields.title] = {
          content: fields && fields.content,
          path: fields.path,
          tags: fields.tags
        }
      } else if (type === 'links') {
        serverData.links.push({
          uri: fields.uri,
          label: fields.label,
          slug: fields.slug,
          tags: fields.tags,
          order: fields.order
        })
      }
    })
    const { tagNames, documentNames } = getTagsAndDocs(serverData, query)
    serverData.tagNames = tagNames
    serverData.documentNames = documentNames
    return serverData
  } catch (e) {
    console.log('error', e)
  }
  return serverData
}

const getTagsAndDocs = (serverData, query) => {
  let tagNames = []
  let documentNames = []
  try {
    tagNames = getTags(serverData.documents)
    documentNames = filterDocuments(serverData.documents, query.tag)
  } catch (e) {
    console.log('error', e)
  }

  return { tagNames, documentNames }
}

const getTags = pageData => {
  let tagSet = new Set()
  Object.keys(pageData).forEach(pageTitle => {
    const pageTags = pageData[pageTitle].tags || []
    pageTags.forEach(tag => tagSet.add(tag))
  })
  let tagList = []
  tagSet.forEach(tag => tagList.push(tag))
  return tagList.sort()
}

const filterDocuments = (pageData, tag) => {
  return Object.keys(pageData).filter(pageTitle =>
    pageData[pageTitle].tags.includes(tag)
  )
}

const getSiteContent = async (req, query) => {
  let props = {}
  props.pages = { homePage: '' }
  props.cookies = parseCookies({ req })
  props.documents = {}
  props.events = []
  try {
    if (!firebase.apps.length) {
      firebase.initializeApp(config.firebase)
    }
    const content = await getBlogContent(query)
    const { pages, documents, tagNames, documentNames } = content
    props.pages = pages
    props.documents = documents
    props.tagNames = tagNames
    props.documentNames = documentNames
    const snapshot = await firebase
      .database()
      .ref('events/')
      .once('value')
    const events = snapshot.val()
    props.events = events
    props.app = { query, content, events }

    return props
  } catch (e) {
    console.log('caught exception', e)
  }
  return props
}

export default getSiteContent
