import React, { Component } from "react"
import { format } from "date-fns"

import Event from "../types/event"

interface Props {
  event: Event;
}

function findRegistrationLink(signupUrl?: string): JSX.Element {
  if (signupUrl) {
    return (
      <p className="mt-0 mb-0">
        <a target="_blank" rel="noopener noreferrer" href={signupUrl}>
          Register here
        </a>
      </p>
    )
  }
  return <strong>Sorry: cannot register for this event</strong>
}

export default class EventData extends Component<Props, {}> {
  public render() {
    const { event } = this.props
    const dfn = event.startAt
    const date = format(dfn, "dddd, MMM Do, YYYY")
    const time = format(dfn, "h:mm a")
    const datetime = `${date} at ${time}`
    return (
      <section
        className="container border-grey border-solid border rounded-sm pl-1 pr-1 mb-2"
        style={{ backgroundColor: "#c7d1da" }}
      >
        <p className="mt-0 text-lg">{event.title}</p>
        <section>
          <p className="text-grey-darker text-base mt-0 mb-0">
            Time: &nbsp; {datetime}
          </p>
          {findRegistrationLink(event.signupUrl)}
        </section>
      </section>
    )
  }
}
