import React from "react"

export enum VoteButtonStatus {
  ready = "ready",
  saving = "saving",
  notReady = "notReady"
}

interface Props {
  onClick: (e: any) => Promise<void>
  status: VoteButtonStatus
}

export default class VoteButton extends React.Component<Props> {
  public render() {
    const { status, onClick } = this.props
    const disabled = status !== VoteButtonStatus.ready
    let buttonClass =
      "font-semibold py-2 px-4 border border-grey-light rounded shadow "
    if (disabled) {
      buttonClass += "bg-grey text-black"
    } else {
      buttonClass += "bg-blue hover:bg-blue-dark text-white"
    }

    let label = status === VoteButtonStatus.saving ? 'Casting ballot...' : 'Cast your vote'

    return (
      <div>
        <button
          onClick={onClick}
          className={buttonClass}
          disabled={disabled}
        >
          {label}
        </button>
      </div>
    )
  }
}
