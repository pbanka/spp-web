import NarrowCard from "./NarrowCard"
import React from "react"

export default function VotingCard() {
  return (
    <NarrowCard
      alt="Voting"
      img="/static/images/thank-you-for-thinking.jpg"
      title="Vote for our board"
    >
      <p>
        We are currently electing new members to the SPP Board of Directors!
      </p>
      <p className="text-grey-darker text-base pt-6">
        There are two seats available to be filled, and six outstanding
        candidates! If you are a member of SPP,
        <a
          className="pl-1"
          href="https://secure.meetup.com/oauth2/authorize?client_id=pmg2jt41d60i0ijur3fk2m5v5f&response_type=code&redirect_uri=https://api.sexpositiveportland.org/spp-oauth-callback"
        >
          log in to the site and vote!
        </a>
      </p>
      <p className="text-grey-darker text-base pt-6">
        Voting will open on November 4th and close on midnight November 18th.
        Election results will be announced on November 19th.
      </p>
    </NarrowCard>
  )
}
