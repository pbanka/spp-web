import React, { Component } from "react"

interface Props {
  open: boolean;
}

export default class Hamburger extends Component<Props, {}> {
  public render() {
    let classNames = "cursor-pointer fixed z-50 white p-2 burger-menu"
    if (this.props.open) {
      classNames += " open"
    }
    return (
      <div className={classNames} {...this.props}>
        <div className="bar1" key="b1" />
        <div className="bar2" key="b2" />
        <div className="bar3" key="b3" />
      </div>
    )
  }
}
