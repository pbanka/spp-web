import {
  BallotInput,
  Candidate,
  Election
} from "../types/vote"

import React from "react"

interface Props {
  election: Election
  ballot: BallotInput
  onClick: (candidate: Candidate) => void
}

export default class VoteCollector extends React.Component<Props> {
  public makeChoice = (candidate: Candidate) => {
    this.props.onClick(candidate)
  };

  public choice = (candidate: Candidate, i: number) => {
    const { election, ballot } = this.props
    const { votes } = ballot
    const checked = votes.map(v => v.candidate_id).includes(candidate.id)
    const disabled = !checked && votes.length >= election.number_of_seats
    const fontColor = disabled ? 'text-gray-500' : ''

    return (
      <li
        key={`${i}${election.slug}`}
        className={`md:flex md:items-center mb-6 ${fontColor}`}
      >
        <input
          className="border-2 border-grey-lighter px-4 mr-2 focus:outline-none focus:bg-white focus:border-purple"
          checked={checked}
          id={`${election.slug}-${candidate.id}`}
          name="candidateId"
          type="checkbox"
          value={candidate.id}
          onChange={() => this.makeChoice(candidate)}
          disabled={disabled}
        />
        <label
          className={`block text-grey-darkest font-bold md:text-right md:mb-0 pr-4 ${fontColor}`}
          htmlFor={`${election.slug}-${candidate.id}`}
        >
          {candidate.name}
        </label>
      </li>
    )
  }

  public render() {
    const { election } = this.props
    const { candidates } = election

    return (
      <div className="border-grey-light border-solid border mb-2">
        <h3 className="p-2 mb-2">
          {election.title}
        </h3>
        <ul className="text-grey-darker text-base">
          {candidates.map(this.choice)}
        </ul>
      </div>
    )
  }
}
