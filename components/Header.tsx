import React, { Component } from "react"

import { ContentType } from "../types/header"
import HamburgerMenu from "./HamburgerMenu"
import Logo from "./Logo"
import NavLink from "./NavLink"

interface Props {
  app: ContentType;
  path: string;
  jwt: string
}

interface State {
  showHamburger: boolean;
  hookCreated: boolean;
  intervalId: string | null;
}

export default class Header extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = { showHamburger: false, hookCreated: false, intervalId: null }
  }

  public filterAndSort(filterString: string) {
    return this.props.app.content.links
      .filter(link => link.tags && link.tags.includes(filterString))
      .sort((a, b) => {
        if (a.order < b.order) {
          return -1
        } else if (a.order > b.order) {
          return 1
        }
        return 0
      })
  }

  public navLinks(filterString: string, extraLinks = []) {
    const links = this.filterAndSort(filterString).concat(extraLinks)
    return links.map(link => {
      const href = link.slug === "pages" ? "/?documents" : link.uri
      return (
        <NavLink
          active={href === this.props.path}
          title={link.label}
          href={href}
          key={link.slug}
        />
      )
    })
  }

  public hamburgerHelper = () => {
    const widthOfAllText = this.props.app.content.links.reduce(
      (acc, link) => acc + link.label.length,
      0
    )
    const showHamburger = widthOfAllText * 6 > window.innerWidth
    this.setState({ showHamburger })
  };

  public componentDidMount() {
    // FIXME: WTF is this?
    setTimeout(this.setHook, 1000)
    if (window) {
      try {
        this.hamburgerHelper.bind(this)
        window.addEventListener("resize", this.hamburgerHelper)
      } catch (e) {
        // tslint:disable-next-line
        console.log("wtf", e)
      }
    }
    this.hamburgerHelper()
  }

  public render() {
    let links = []
    if (this.props.jwt) {
      links = [
        { uri: "/events", label: "Public Events" },
        { uri: "/logout", label: "Log out" },
        { uri: "/members", label: "Members" }
      ]
    } else {
      links = [
        { uri: "/events", label: "Public Events" },
        { uri: "/login", label: "Log in" },
        { uri: "/join", label: "Join SPP!" }
      ]
    }
    const rightHandNav = links.map(link => {
      return (
        <NavLink
          title={link.label}
          href={link.uri}
          key={link.uri}
          active={link.uri === this.props.path}
        />
      )
    })

    let primaryNav
    if (this.state.showHamburger === false) {
      primaryNav = (
        <div className="flex" style={{ width: "calc(100% - 400px)" }}>
          {this.navLinks("secondary-nav")}
        </div>
      )
    } else {
      primaryNav = (
        <HamburgerMenu>{this.navLinks("secondary-nav")}</HamburgerMenu>
      )
    }

    return (
      <div className="flex w-full bg-spp-light-grey flex-row justify-center">
        <header
          className="w-full max-w-lg bg-spp-light-grey z-10 flex flex-col justify-between"
          style={{ gridArea: "header" }}
        >
          <Logo />
          <nav className="mr-4 ml-4 flex flex-row justify-between">
            <div>
              {/* TODO: remove classless wrapping div */}
              {primaryNav}
              <div className="flex items-end">{rightHandNav}</div>
            </div>
          </nav>
          <div className='container bg-spp-violet mt-4 mx-4 px-4 text-spp-light-grey rounded-full flex flex-row justify-center'>
            <h3>The SPP Board election is going on now. <a href="/election" className="text-white">Please vote!</a></h3>
          </div>
        </header>
      </div>
    )
  }

  private setHook = () => undefined; // FIXME: WTF is this?
}
