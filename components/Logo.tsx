import React, { Component } from "react"

import Link from "next/link"

export default class Logo extends Component {
  public render() {
    return (
      <div className="mr-4 ml-4 mt-4 flex justify-left">
        <Link href="/">
          <a className="text-spp-dark font-logo">
            <span className="logo">X</span>
            <span className="logo text-spp-violet">+</span>
            <div className="chapter text-spp-violet font-header">portland</div>
          </a>
        </Link>
      </div>
    )
  }
}
