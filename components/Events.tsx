import EventData from "./EventData"
import Event from "../types/event"
import NarrowCard from "./NarrowCard"
import React from "react"

interface Props {
  events: Event[]
}

class Events extends React.Component<Props, {}> {
  public render() {
    if (!this.props.events) {
      return null
    }

    const awesomeBoundaries = this.props.events.filter(
      event => event.title.toLowerCase().includes('awesome boundaries')
    )
    const publicDancesPicnicsParties = this.props.events.filter(
      event => {
        const title = event.title.toLowerCase()
        return title.includes('public dance') || title.includes('public picnic') || title.includes('public party')
      }
    )
    const publicSocials = this.props.events.filter(
      event => {
        const title = event.title.toLowerCase()
        return title.includes('coffee social') || title.includes('wine social')
      }
    )

    return (
      <>
        <div className="w-auto flex flex-wrap justify-center">
          <NarrowCard
            addedClasses=""
            alt="Socials"
            img="/static/images/coffee-social.jpg"
            title="Public Socials"
          >
            <p className="text-grey-darker text-base">
              SPP has a number of social events throughout the month for new and
              existing members. We are now offering a few slots to these social
              events for non-members. This is a good way to learn more about SPP
              and find out who we are and what we do!
            </p>
            <p className="pt-1 text-grey-darker text-base">
              To come to one of our social events, you must be either a member,
              or you must have a ticket!
            </p>
            {this.listEvents(publicSocials)}
          </NarrowCard>
          <NarrowCard
            addedClasses=""
            alt="Awesome Boundaries"
            img="https://images.ctfassets.net/z60c05cc1iui/6l3RyElhyi3tCC6QxFchML/a011295c1d96af836371f26a43a30fa5/awesome-boundaries.jpeg"
            title="Public Awesome Boundaries"
          >
            <p className="text-grey-darker text-base">
              Do you know how to affirm your boundaries?
              Do you even know clearly what your boundaries are?
              Most of us don't. Good boundaries lead to higher trust and higher safety - both of which lead to better sex!
              This class will explore social AND sexual boundaries - how to establish them, respect them, and have others respect them.
            </p>
            {this.listEvents(awesomeBoundaries)}
          </NarrowCard>
          <NarrowCard
            addedClasses=""
            alt="Dances"
            img="/static/images/women-dancing.jpg"
            title="Public Dances, Picnics and Parties"
          >
            <p className="text-grey-darker text-base">
              We have dances, parties and picnics that are open to the public. Register here if you want to attend.
            </p>
            {this.listEvents(publicDancesPicnicsParties)}
          </NarrowCard>
        </div>
      </>
    )
  }

  private listEvents(events: Event[]) {
    if (events.length === 0) {
      return <p>There are no events posted. Please check back later.</p>
    }
    return events.map(event => <EventData key={event.id} event={event} />)
  }
}

export default Events
