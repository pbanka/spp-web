import { DocumentType } from "../types/document"
import React from "react"
import ReactMarkdown from "react-markdown"

interface Props {
  documents: DocumentType;
  documentNames: string[];
  tag: string;
  title: string;
}

export default class Stories extends React.Component<Props, {}> {
  public render() {
    const tag = this.props.tag
    const title = this.props.title

    let docList: any = []
    let content = ""
    if (title) {
      content = this.props.documents[title].content
    }
    try {
      docList = this.props.documentNames.map(documentName => (
        <li key={documentName}>
          <a href={`/?documents&title=${encodeURI(documentName)}&tag=${tag}`}>
            {documentName}
          </a>
        </li>
      ))
    } catch (e) {
      // tslint:disable-next-line
      console.log("error", e)
    }
    return (
      <div>
        <section className="story container h-full flex flex-row">
          <div>
            <h1>Documents</h1>
            <ul>{docList}</ul>
          </div>
        </section>
        <section className="story container h-full border-grey-dark border-solid border-t pt-4 mt-4 mr-4">
          <h1 className="mb-4">{title}</h1>
          <ReactMarkdown source={content} />
        </section>
      </div>
    )
  }
}
