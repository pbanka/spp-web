import React, { Component } from "react"

import MediaQuery from "react-responsive"
import NarrowCard from "./NarrowCard"
import WideCard from "./WideCard"

interface Props {
  className?: string;
  alt: string;
  title: string;
  img: string;
}

export default class Card extends Component<Props, {}> {
  public render() {
    return (
      <>
        <MediaQuery minWidth={1024}>
          <WideCard
            addedClasses={this.props.className || ""}
            title={this.props.title}
            img={this.props.img}
            children={this.props.children}
          />
        </MediaQuery>
        <MediaQuery maxWidth={1024}>
          <NarrowCard
            addedClasses={this.props.className || ""}
            alt={this.props.alt}
            title={this.props.title}
            img={this.props.img}
            children={this.props.children}
          />
        </MediaQuery>
      </>
    )
  }
}
