import React from "react"
import Router from "next/router"

interface Props {
}

class LoginContainer extends React.Component<Props> {
  constructor(props: Props) {
    super(props)
  }

  public onClick = (e: any) => {
    e.preventDefault()
    e.stopPropagation()
    Router.push("/dev_login_create")
  }

  public content() {
    return <div>
      <h1> Development Test Login </h1>
      <p>
        <button className="bg-blue hover:bg-blue-dark text-white font-semibold py-2 px-4 border border-grey-light rounded shadow" onClick={this.onClick}>
          Simulate Login
        </button>
      </p>
    </div>
  }

  public render() {
    return <div className="w-auto flex flex-wrap justify-center">
      {this.content()}
    </div>
  }
}

export default LoginContainer
