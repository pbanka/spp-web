import { HomepageContentType } from "../types/document"
import PostContainer from "./PostContainer"
import React from "react"

interface Props {
  posts: HomepageContentType[];
}

class Home extends React.Component<Props, {}> {
  public render() {
    return (
      <PostContainer category="home" posts={this.props.posts}>
        <div className="flex justify-center m-4 w-full">
          <div className="max-w-xl m-6 items-center">
            <div className="font-bold text-xl mb-2 text-center">
              Want to join us?
            </div>
            <p className="text-grey-darker text-base">
              <a
                className="bg-orange-dark hover:bg-orange text-almost-black font-bold py-2 px-4 rounded max-w-sm uppercase"
                href="/join"
              >
                · Request to Join ·
              </a>
            </p>
          </div>
        </div>
      </PostContainer>
    )
  }
}

export default Home
