import React, { Component } from "react"

interface Props {
  addedClasses: string;
  img: string;
  title: string;
}

export default class WideCard extends Component<Props> {
  public render() {
    const addedClasses = this.props.addedClasses
    return (
      <section
        style={{
          backgroundImage: `url(${this.props.img})`,
          backgroundPosition: "center center",
          backgroundRepeat: "no-repeat",
          height: "90vh"
        }}
        className={`w-full mt-4 flex items-center justify-end flex-row ${addedClasses}`}
      >
        <div className="px-6 py-2 max-w-sm bg-spp-light-grey mr-4 opacity-90 rounded">
          <div className="font-bold text-xl mb-2">{this.props.title}</div>
          {this.props.children}
        </div>
      </section>
    )
  }
}
