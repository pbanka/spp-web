import React from "react"
import { BlockType } from "../types/document"

interface Props {
  blocks: BlockType[]
}

export default class VotingBanner extends React.Component<Props> {
  public render() {
    const block = this.props.blocks.find(b => b.slug === 'board-election-form')
    const body = block && block.body || '(no content)'

    return <div dangerouslySetInnerHTML={{ __html: body }} />
  }
}
