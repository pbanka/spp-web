import { ContentType, LinkType } from "../types/header"
import { SingletonRouter, withRouter } from "next/router"

import React from "react"

interface Props {
  app: ContentType;
  router: SingletonRouter;
  section: string;
  type: string;
}

class Navbar extends React.Component<Props> {
  public navItemsForCurrentSection(allLinks: LinkType[]) {
    const { section, type } = this.props
    const { asPath } = this.props.router

    return allLinks
      .filter(
        link =>
          link.tags &&
          link.tags.includes(`section-${section}`) &&
          link.tags.includes(`${type}-nav`)
      )
      .sort((a, b) => {
        if (a.order < b.order) {
          return -1
        }
        if (a.order > b.order) {
          return 1
        }
        return 0
      })
      .map(link => ({ ...link, active: link.uri === asPath }))
  }

  public render() {
    const navItems = this.navItemsForCurrentSection(
      this.props.app.content.links
    )
    if (!navItems.length) {
      return null
    }

    return (
      <div
        className="pl-2 mt-2 pb-2 flex flex-col bg-indigo-light"
        style={{ gridArea: "secondary-nav" }}
      >
        <h3>Select a section:</h3>
        <ul className="list-reset flex flex-row mb-2">
          {navItems.map(item => (
            <li className="pr-2 mr-3" key={item.slug}>
              <a
                className={`${
                  item.active
                    ? "font-black border-solid border-white border-b"
                    : ""
                  }`}
                href={item.uri}
              >
                {item.label}
              </a>
            </li>
          ))}
        </ul>
      </div>
    )
  }
}

const anyNavbar = Navbar as any
export default withRouter(anyNavbar)
