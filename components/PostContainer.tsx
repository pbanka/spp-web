import { HomepageContentType } from "../types/document"
import NarrowCard from "./NarrowCard"
import React from "react"

interface Props {
  posts: HomepageContentType[];
  category: string;
}

const IMAGE_BASE = "https://"

class PostContainer extends React.Component<Props, {}> {
  public render() {
    // TODO: alt on image
    if (!this.props.posts) {
      return null
    }
    const content = this.props.posts
      .filter(post => post.category === this.props.category)
      .map(post => (
        <NarrowCard
          key={post.title}
          alt=""
          img={`${IMAGE_BASE}${post.image}`}
          title={post.title}
        >
          <p
            className="text-grey-darker text-base"
            dangerouslySetInnerHTML={{ __html: post.body }}
          />
          <p
            className="pt-4"
            dangerouslySetInnerHTML={{ __html: post.secondaryBody }}
          />
        </NarrowCard>
      ))
    return (
      <div className="w-auto flex flex-wrap justify-center">
        {content}
        {this.props.children}
      </div>
    )
  }
}

export default PostContainer
