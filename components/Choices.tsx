import React, { Component } from "react"

interface DataType {
  value: string;
  label: string;
}

interface Props {
  data: DataType[];
  type: string;
  name: string;
  other: boolean;
  row: boolean;
  question: string;
}

interface State {
  other: boolean;
}

export default class Choices extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = { other: false }
  }

  public render() {
    const options = this.props.data.map(choice => {
      const value = choice.value || choice.label.toLowerCase()
      const name = this.props.type === "radio" ? this.props.name : value
      return (
        <div key={value} className="mb-1 mr-2">
          <input
            className="mr-4"
            type={this.props.type || "checkbox"}
            name={name}
            value={value}
          />
          <label htmlFor="value">{choice.label}</label>
        </div>
      )
    })

    if (this.props.other) {
      let other
      if (this.state.other) {
        other = (
          <input
            className="mr-4 ml-4 border-t border-b border-grey-dark border-solid "
            type="text"
            name="other-data"
          />
        )
      }
      options.push(
        <div key="other" className="mb-1 mr-4">
          <input
            onClick={() => this.toggleOther()}
            className="mr-4"
            type={this.props.type || "checkbox"}
            name="other"
            value="other"
          />
          <label htmlFor="other">Other</label>
          {other}
        </div>
      )
    }

    const orientation = this.props.row ? "flex-row" : "flex-col"
    return (
      <div className="flex flex-col mt-8">
        <label className="font-sans text-2xl font-thin" htmlFor="why-to-join">
          {this.props.question}
        </label>
        <div className={`flex ${orientation} m-4`}>{options}</div>
      </div>
    )
  }

  private toggleOther = () => {
    this.setState({ other: !this.state.other })
  };
}
