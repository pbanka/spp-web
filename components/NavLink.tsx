import React, { Component } from "react"

import Link from "next/link"

interface Props {
  active?: boolean;
  className?: string;
  href: string;
  img?: string;
  title: string;
}

export default class NavLink extends Component<Props> {
  public render() {
    const image = this.props.img ? (
      <img src={this.props.img} style={{ width: 69, height: 69 }} />
    ) : null
    const anchorClasses = [
      "flex",
      "justify-center",
      "nav-item",
      "px-2",
      "no-underline",
      "text-spp-dark",
      "font-header",
      "text-m",
      "hover:text-black",
      "hover:bg-spp-light-grey",
      this.props.className,
      this.props.active
        ? "text-white bg-spp-violet hover:text-white hover:bg-spp-violet-2"
        : ""
    ].join(" ")
    return (
      <Link href={this.props.href}>
        <a
          style={{ transitionProperty: "background-color", transition: "0.2s" }}
          className={anchorClasses}
        >
          {image}

          {this.props.title}
        </a>
      </Link>
    )
  }
}
