import React, { Component } from "react"

import HamburgerIcon from "./HamburgerIcon"
import Popup from "reactjs-popup"

export default class HamburgerMenu extends Component {
  public render() {
    return (
      <div className="flex items-end">
        <Popup
          position="bottom left"
          on="click"
          closeOnDocumentClick
          mouseLeaveDelay={300}
          mouseEnterDelay={0}
          contentStyle={{
            background: "black",
            lineHeight: 2,
            marginLeft: 10,
            paddingLeft: 10
          }}
          arrow={false}
          overlayStyle={{ background: "rgba(0, 0, 0, 0.98)" }}
          trigger={open => <HamburgerIcon open={open} />}
        >
          <div className="flex flex-col">{this.props.children}</div>
        </Popup>
      </div>
    )
  }
}
