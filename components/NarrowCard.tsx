import React, { Component } from "react"

interface Props {
  addedClasses?: string;
  alt: string;
  img?: string;
  title: string;
}

export default class NarrowCard extends Component<Props> {
  public render() {
    const addedClasses = this.props.addedClasses || ""
    return (
      <section className={`max-w-sm mt-4 mr-4 ml-4 shadow-lg ${addedClasses}`}>
        <figure className="w-full justify-center flex-col max-h-md flex overflow-hidden">
          {this.props.img && <img className="w-full" alt={this.props.alt} src={this.props.img} />}
        </figure>
        <div className="px-6 py-4">
          <div className="font-bold text-xl mb-2">{this.props.title}</div>
          {this.props.children}
        </div>
      </section>
    )
  }
}
