import { SingletonRouter, withRouter } from "next/router"

import { PageType } from "../types/page"
import React from "react"
import ReactMarkdown from "react-markdown"

const FAVICON = "/favicon.ico"

interface Props {
  pages: PageType;
  router: SingletonRouter;
  storyPath: string;
}

class Stories extends React.Component<Props> {
  public render() {
    const query = this.props.router.query || { storyPath: "" }
    let storyPath =
      query.storyPath ||
      this.props.storyPath ||
      this.props.router.asPath ||
      ("" as any) // TS getting confused
    storyPath = storyPath.replace(/\?.*/, "")
    if (storyPath === FAVICON) {
      return null
    }

    storyPath = storyPath === "/" ? "home" : storyPath

    const storyName = Object.keys(this.props.pages).find(title => {
      const path = this.props.pages[title].path
      return path === storyPath
    })

    if (!storyName) {
      // tslint:disable-next-line
      console.log(">> NOTHING FOUND FOR", storyPath)
      if (typeof window !== "undefined") {
        (window as any).location.replace("https://www.sexpositiveportland.org")
      }
      return null
    }

    const content = this.props.pages[storyName].content
    return (
      <section className="flex justify-center h-full">
        <div className="max-w-md p-4">
          <ReactMarkdown source={content} />
        </div>
      </section>
    )
  }
}

const anyStories = Stories as any
export default withRouter(anyStories)
