import { ContentType } from "../types/header"
import { SingletonRouter, withRouter } from "next/router"

import Head from "next/head"
import Header from "./Header"
import Navbar from "./Navbar"
import React from "react"

const DumbNavbar = Navbar as any // Next Router screws up typing

// Typescript needs us to use require() for css files
const stylesheet = require("../styles/styles.css") // eslint-disable-line

interface Props {
  router: SingletonRouter
  title: string
  app: ContentType
  jwt: string
  tagNames: string[]
}

interface State {
  navOpen: boolean;
  modal: boolean;
}

class Layout extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      modal: true,
      navOpen: false
    }
  }

  public render() {
    const path = this.props.router.asPath || ""
    let secondarySelection = "none"
    const match = path.match(/^\/(\w*)/)
    if (match) {
      secondarySelection = match[1]
    }

    return (
      <>
        <Head>
          <meta charSet="UTF-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <title>{this.props.title || "SexPositive Portland"}</title>
          <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
          <script src="https://cdn.polyfill.io/v2/polyfill.min.js" />
        </Head>
        <div className="font-sans flex flex-1 flex-col h-full">
          <Header
            app={this.props.app}
            jwt={this.props.jwt}
            path={path}
          />
          <DumbNavbar
            app={this.props.app}
            type="tertiary"
            section={secondarySelection}
          />
          {this.props.children}
        </div>
      </>
    )
  }
}

const AnyLayout = Layout as any
export default withRouter(AnyLayout)
