import {
  Ballot,
  BallotInput,
  CandidateInput,
  Election,
  ElectionStatus
} from "../types/vote.d"
import { postMyBallot } from "../lib/spp-api"
import NarrowCard from "./NarrowCard"
import VoteOptions from "./VoteOptions"
import React from "react"
import VoteButton, { VoteButtonStatus } from "./VoteButton"
import VotingBanner from "./VotingBanner"
import { BlockType } from "../types/document"

interface Props {
  election?: Election
  ballots: Ballot[]
  jwt: string
  blocks: BlockType[]
}

interface State {
  // Maps a vote_id to a list of candidate names
  ballot?: BallotInput
  votingError: boolean
  saving: boolean
  savedBallot?: Ballot
}

export default class ElectionCard extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    const { election } = props
    const ballot = election ? { election_slug: election.slug, votes: [] } : undefined

    this.state = {
      ballot,
      votingError: false,
      saving: false,
    }
  }

  public handleVoteSubmit = async (e: Event) => {
    e.preventDefault()
    e.stopPropagation()
    const { ballot } = this.state
    if (!ballot) throw new Error("ballot is undefined")
    this.setState({ saving: true }, async () => {
      try {
        const savedBallot = await postMyBallot(ballot, this.props.jwt)
        this.setState({ savedBallot, saving: false })
      } catch (error) {
        console.warn('error', error)
        this.setState({ saving: false, votingError: true })
      }
    })
  }

  public displayVotingError() {
    if (!this.state.votingError) {
      return null
    }
    return (
      <p style={{ color: "red" }}>
        There was a problem recording your vote. Please try again in a little
        while. If the problem persists, please contact
        <a href="https://www.meetup.com/sexpositivepdx/members/133528312/"> Chirsty Staats </a>
      </p>
    )
  }

  public candidateSelected(candidate: CandidateInput) {
    this.setState(state => {
      const { ballot } = state
      if (!ballot) throw "ballot is undefined"
      const isVoted = ballot.votes.map(v => v.candidate_id).includes(candidate.id)
      let votes
      if (isVoted) {
        votes = ballot.votes.filter(v => v.candidate_id != candidate.id)
      } else {
        votes = [...ballot.votes, { candidate_id: candidate.id }]
      }
      return { ballot: { ...ballot, votes } }
    })
    return null
  }

  public voteButton(election: Election, ballot: BallotInput) {
    if (election.candidates.length === 0) {
      return null
    }

    let status: VoteButtonStatus
    if (ballot.votes.length === 0) {
      status = VoteButtonStatus.notReady
    } else if (this.state.saving) {
      status = VoteButtonStatus.saving
    } else {
      status = VoteButtonStatus.ready
    }

    return <VoteButton
      onClick={this.handleVoteSubmit}
      status={status}
    />
  }

  public chooseNCandidatesMessage(election: Election, ballot: BallotInput) {
    const remaining = election.number_of_seats - ballot.votes.length
    const candidates = remaining > 1 ? 'candidates' : 'candidate'
    if (remaining === 0) {
      return null
    }
    const more = ballot.votes.length > 0 ? 'more' : ''
    return <p className="flex justify-center">
      Choose {remaining} {more} {candidates}
    </p>
  }

  public electionStatus(election: Election): ElectionStatus {
    const now = Date.now()
    if (election.opened_at) {
      const openedAt = Date.parse(election.opened_at)
      if (now < openedAt) {
        return ElectionStatus.NotOpen
      }
    }

    if (election.closed_at) {
      const closedAt = Date.parse(election.closed_at)
      if (now > closedAt) {
        return ElectionStatus.Closed
      }
    }

    return ElectionStatus.Open
  }

  public content() {
    const { election, ballots, jwt } = this.props
    const { savedBallot } = this.state

    if (!jwt) {
      return <p>
        Please <a href="/login?return=/election">log in</a> to vote.
      </p>
    }

    if (savedBallot) {
      return <div>
        <p>
          Your ballot has been cast. Thank you for voting!<br />
        </p>
        <p>
          Ballot ID {savedBallot.id}
        </p>
      </div>
    }

    if (!election) {
      return <p>The election is now closed.</p>
    }

    const castBallot = ballots.find(b => b.election_slug === election.slug)

    if (castBallot) {
      return <p>We've received your vote. Thank you for voting!</p>
    }

    const status = this.electionStatus(election)
    if (status === ElectionStatus.NotOpen) {
      return <p>The election is not yet open for voting.</p>
    }
    if (status === ElectionStatus.Closed) {
      return <p>The election is now closed to voting.</p>
    }

    const { ballot } = this.state
    if (!ballot) throw "ballot is undefined"

    return <form className="w-full max-w">
      <VoteOptions
        election={election}
        ballot={ballot}
        onClick={c => this.candidateSelected(c)}
      />
      {this.chooseNCandidatesMessage(election, ballot)}
      <div className="flex justify-center">
        {this.voteButton(election, ballot)}
      </div>
      {this.displayVotingError()}
    </form>
  }

  public render() {
    const { blocks } = this.props

    return (
      <NarrowCard
        alt="Vote"
        img="/static/images/thank-you-for-thinking.jpg"
        title="Vote for our board"
      >
        <VotingBanner blocks={blocks} />
        {this.content()}
      </NarrowCard>
    )
  }
}
