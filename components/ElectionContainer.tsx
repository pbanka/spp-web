import React from "react"
import ElectionCard from "./ElectionCard"
import { Ballot, Election } from "../types/vote"
import { BlockType } from "../types/document"

interface Props {
  elections: Election[]
  ballots: Ballot[]
  jwt: string
  blocks: BlockType[]
}

class ElectionContainer extends React.Component<Props, {}> {
  constructor(props: Props) {
    super(props)
  }

  public content() {
    const { elections, ballots, jwt, blocks } = this.props;
    const election = elections.find(e => e.slug === 'test-01')

    return <ElectionCard election={election} ballots={ballots} jwt={jwt} blocks={blocks} />
  }

  public render() {

    return (
      <div className="w-auto flex flex-wrap justify-center">
        {this.content()}
        {this.props.children}
      </div>
    )
  }
}

export default ElectionContainer
