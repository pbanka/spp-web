/* eslint-disable @typescript-eslint/no-var-requires */
"use strict"

const next = require("next")
const nextAuth = require("next-auth")
const nextAuthConfig = require("./next-auth.config")
var cookieParser = require('cookie-parser')

const nextRoutes = {
  admin: require("./routes/admin"),
  account: require("./routes/account")
}

// Load environment variables from .env file if present
require("dotenv").load()

const Honeybadger = require("honeybadger").configure({
  apiKey: "d8c6cbc7"
})

// now-logs allows remote debugging if deploying to now.sh
if (process.env.LOGS_SECRET) {
  require("now-logs")(process.env.LOGS_SECRET)
}

process.on("uncaughtException", function (err) {
  console.error("Uncaught Exception: ", err)
  Honeybadger.notify(err)
})

process.on("unhandledRejection", (reason, p) => {
  console.error("Unhandled Rejection: Promise:", p, "Reason:", reason)
  Honeybadger.notify({ promise: p, reason: reason })
})

// Default when run with `npm start` is "production" and default port is "80"
// `npm run dev` defaults mode to "development" & port to "3000"
process.env.NODE_ENV = process.env.NODE_ENV || "production"
process.env.PORT = process.env.PORT || 80

// Initialize Next.js
const nextApp = next({
  dir: ".",
  dev: (process.env.NODE_ENV === "development")
})

const handle = nextApp.getRequestHandler()

// Add next-auth to next app
nextApp
  .prepare()
  .then(() => {
    // Load configuration and return config object
    return nextAuthConfig()
  })
  .then(nextAuthOptions => {
    // Pass Next.js App instance and NextAuth options to NextAuth
    // Note We do not pass a port in nextAuthOptions, because we want to add some
    // additional routes before Express starts (if you do pass a port, NextAuth
    // tells NextApp to handle default routing and starts Express automatically).
    return nextAuth(nextApp, nextAuthOptions)
  })
  .then(nextAuthOptions => {
    // Get Express and instance of Express from NextAuth
    const express = nextAuthOptions.express
    const expressApp = nextAuthOptions.expressApp

    // In order to function properly, Honeybadger"s middleware must be added
    // _before_ and _after_ the normal app middleware, but before any other error
    // handling middleware
    expressApp.use(Honeybadger.requestHandler)

    // Add admin routes
    nextRoutes.admin(expressApp)

    // Add account management route - reuses functions defined for NextAuth
    nextRoutes.account(expressApp, nextAuthOptions.functions)

    // Serve fonts from ionicon npm module
    expressApp.use("/fonts/ionicons", express.static("./node_modules/ionicons/dist/fonts"))

    // Parse cookies automatically
    expressApp.use(cookieParser())

    // // A simple example of custom routing
    // // Send requests for "/custom-route/{anything}" to "pages/examples/routing.js"
    // expressApp.get("/custom-route/:id", (req, res) => {
    //   return nextApp.render(req, res, "/examples/routing", req.params)
    // })

    // Default catch-all handler to allow Next.js to handle all other routes
    expressApp.all("*", (req, res) => {
      // Custom routing that routes everyting to /pages/index.tsx
      return nextApp.render(req, res, "/index", req.query)

      // Normal NextJS Routing
      // return handle(req, res)
    })

    expressApp.listen(process.env.PORT, err => {
      if (err) {
        throw err
      }
      console.log("> Ready on http://localhost:" + process.env.PORT + " [" + process.env.NODE_ENV + "]")
    })

    // Add Honeybadger middleware _after_ all other middleware. See explanation above.
    expressApp.use(Honeybadger.errorHandler)
  })
  .catch(err => {
    console.log("An error occurred, unable to start the server")
    console.log(err)
  })
