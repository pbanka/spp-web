module.exports = {
  webpack: (config) => {
    config.module.rules.push(
      {
        test: /\.(css|scss)/,
        loader: "emit-file-loader",
        options: {
          name: "dist/[path][name].[ext]"
        }
      },
      {
        test: /\.css$/,
        use: ["babel-loader", "raw-loader", "postcss-loader"]
      },
      {
        test: /\.scss$/,
        loader: "babel-loader!raw-loader!sass-loader"
      }
    )

    return config
  },
  env: {
    // These environment variables, such as SPP_API_URL can be overridden in your .env file
    sppApiBaseUrl: process.env.SPP_API_URL || "https://api.sexpositiveportland.org/api",
    meetupOauthClientId: process.env.MEETUP_OAUTH_CLIENT_ID || "pmg2jt41d60i0ijur3fk2m5v5f",
    meetupOauthRedirectUrl: process.env.MEETUP_OAUTH_REDIRECT_URL || "https://api.sexpositiveportland.org/spp-oauth-callback"
  }
}
