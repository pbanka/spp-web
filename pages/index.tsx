import { DocumentType, HomepageContentType, BlockType } from "../types/document"
import Router, { SingletonRouter, withRouter } from "next/router"

import { AppType, NextJsContext, RequestType, ResponseType } from "../types/system"
import Documents from "../components/Documents"
import Events from "../components/Events"
import Home from "../components/Home"
import Layout from "../components/Layout"
import { PageType } from "../types/page"
import PostContainer from "../components/PostContainer"
import LoginContainer from "../components/LoginContainer"
import ElectionContainer from "../components/ElectionContainer"
import Event from "../types/event"
import React from "react"
import Story from "../components/Story"
import { Ballot, Election } from "../types/vote"
import digOutValue from "../lib/dig-out-value"
import getSiteContent from "../lib/get-site-content"
import { parseCookies } from "nookies"

const RoutedStory = Story as any // Next router screws up typing
const RoutedLayout = Layout as any // Next router screws up typing

const redirect = (res: ResponseType, path?: string) => {
  path = path || "/"
  const inBrowser = !res
  if (inBrowser) {
    Router.push(path)
  } else {
    res.writeHead(302, { Location: path })
    res.end()
  }
  return {}
}

interface Props {
  app: AppType
  documents: DocumentType
  documentNames: string[]
  jwt: string
  pages: PageType
  posts: HomepageContentType[]
  router: SingletonRouter
  tagNames: string[]
  ballots: Ballot[]
  elections: Election[]
  blocks: BlockType[]
  events: Event[]
}

const clientId = process.env.meetupOauthClientId
const redirectUrl = process.env.meetupOauthRedirectUrl
const MEETUP_OAUTH_URL =
  `https://secure.meetup.com/oauth2/authorize?client_id=${clientId}&response_type=code&redirect_uri=${redirectUrl}`

const redirectMaybeToSavedLocation = (req: RequestType, res: ResponseType) => {
  res.cookie('spp', process.env.JWT, {})
  const returnTo = req.cookies.return || "/"
  res.clearCookie('return')
  return redirect(res, returnTo)
}

class Index extends React.Component<Props> {
  public static async getInitialProps(context: NextJsContext) {
    const { req, query, res } = context
    const inBrowser = !req
    const path = inBrowser ? window.location.pathname : req.originalUrl
    console.log('Handling request for path', path)

    if (path.startsWith("/favicon.ico")) {
      console.log('Handling favicon.ico')
      res.status(404).send('Not found')
      res.end()
      return
    }

    // Maybe return to saved location
    if (req && req.cookies && req.cookies.return) {
      return redirectMaybeToSavedLocation(req, res)
    }

    // Alternative Authorization for Local Development, using impersonation with a test account
    if (path.startsWith("/dev_login_create")) {
      if (!process.env.JWT) {
        const msg = "No JWT set. See README.md for test authorization using impersonation"
        console.error(msg)
        throw new Error(msg)
      }
      return redirectMaybeToSavedLocation(req, res)
    }

    const cookies = parseCookies({ req })
    const jwt = cookies.spp

    if (path.startsWith("/login")) {
      console.log('Handling /login')
      const parsedPath = new URL('http://localhost' + path)
      const returnTo = parsedPath.searchParams.get('return')

      if (returnTo) {
        // Save this location, so that system will automatically return the agent to it after login
        res.cookie('return', returnTo, {})
      }

      // Unless impersonation is being used, redirect to Meetup's OAuth URL
      if (!process.env.JWT) {
        return redirect(res, MEETUP_OAUTH_URL)
      }
    } else if (path.startsWith("/members") && !jwt) {
      return redirect(res, "/")
    } else if (path.startsWith("/logout")) {
      res.clearCookie('spp')
      return redirect(res, "/")
    }

    let content = {}
    try {
      content = await getSiteContent(query, jwt)
    } catch (error) {
      console.warn("Error fetching site content:", error)
    }

    return { ...content, jwt }
  }

  public render() {
    let child = {}
    const { query, asPath } = this.props.router
    const safeQuery = query || { tag: "", title: "" }
    const tag = digOutValue(query, "tag")
    const title = digOutValue(query, "title")
    const safeAsPath = asPath || ""
    const path = safeAsPath.replace(/\?.*/, "")

    if (tag || safeQuery.title) {
      child = <Documents
        documents={this.props.documents}
        documentNames={this.props.documentNames}
        tag={tag}
        title={title}
      />
    } else if (path === "/") {
      child = <Home posts={this.props.posts} />
    } else if (path === "/login") {
      child = <LoginContainer />
    } else if (path === "/events") {
      child = <Events events={this.props.events} />
    } else if (path === "/election") {
      const { elections, ballots, jwt, blocks } = this.props
      child = <ElectionContainer elections={elections} ballots={ballots} jwt={jwt} blocks={blocks} />
    } else if (path === "/members") {
      child = <PostContainer category="members" posts={this.props.posts} />
    } else {
      child = <RoutedStory pages={this.props.pages} />
    }

    return (
      <RoutedLayout
        tagNames={this.props.tagNames}
        pages={this.props.pages}
        app={this.props.app}
        jwt={this.props.jwt}
      >
        {child}
      </RoutedLayout>
    )
  }
}

export default withRouter(Index)
