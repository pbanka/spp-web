import Document, { Head, Main, NextScript } from "next/document"

import React from "react"

export default class DefaultDocument extends Document {
  static async getInitialProps(ctx) {
    return Document.getInitialProps(ctx)
  }

  render() {
    /**
    * Here we use _document.js to add a "lang" property to the HTML object if
    * one is set on the page.
    **/
    return (
      <html lang={this.props.__NEXT_DATA__.props.lang || "en"}>
        <Head>
          <link rel="stylesheet" href="https://use.typekit.net/gcb7nls.css" />
          <script dangerouslySetInnerHTML={{ __html: "try{Typekit.load({ async: false });}catch(e){}" }} />
        </Head>
        <body>
          {this.props.customValue}
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}
