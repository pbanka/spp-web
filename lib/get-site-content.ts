import * as contentful from "contentful"

import {
  ContentType,
  DocumentType,
  HomepageContentType,
  HomepageField,
  BlockType
} from "../types/document"

import { QueryType } from "../types/query"
import digOutValue from "../lib/dig-out-value"
import { getMyBallots, getElections, getEvents } from "../lib/spp-api"
import { Ballot, Election } from "../types/vote"

const config = require("../config") // eslint-disable-line

const client = contentful.createClient(config.contentful)

const POSTS = "2wKn6yEnZewu2SCCkus4as"

interface FieldType {
  content: string
  uri: string
  label: string
  slug: string
  path: string
  tags: string[]
  order: number
  title: string
}

// Output of client.getEntries(), but not used
// interface ContentfulStories {
//   items: SysType
//   fields: FieldType
// }

interface LinkType {
  uri: string
  label: string
  slug: string
  tags: string[]
  order: number
}

interface ContentfulServerData {
  documents: DocumentType
  documentNames: string[]
  links: LinkType[]
  pages: DocumentType
  tagNames: string[]
  posts: HomepageContentType[]
  blocks: BlockType[]
}

const getTags = (pageData: DocumentType) => {
  const tagSet: Set<string> = new Set()
  Object.keys(pageData).forEach(pageTitle => {
    const pageTags = pageData[pageTitle].tags || []
    pageTags.forEach(tag => tagSet.add(tag))
  })
  const tagList: string[] = []
  tagSet.forEach(tag => tagList.push(tag))
  return tagList.sort()
}

const filterDocuments = (pageData: DocumentType, query: QueryType) => {
  const tag = digOutValue(query, "tag")
  return Object.keys(pageData).filter(pageTitle => {
    const tags = pageData[pageTitle].tags || []
    return tags.includes(tag)
  })
}

async function getBlogContent(query: QueryType): Promise<ContentfulServerData> {
  const serverData = {
    documentNames: [],
    documents: {},
    links: [],
    pages: {},
    posts: [],
    tagNames: [],
    blocks: []
  } as ContentfulServerData

  try {
    const links = await client.getEntries({
      content_type: "link", // eslint-disable-line
      limit: 1000
    })
    serverData.links = links.items.map(link => {
      const fields = link.fields as FieldType
      return {
        label: fields.label,
        order: fields.order,
        slug: fields.slug,
        tags: fields.tags,
        uri: fields.uri
      } as LinkType
    })

    const pages = await client.getEntries({
      content_type: "page", // eslint-disable-line
      limit: 1000
    })
    serverData.pages = pages.items.reduce((memo: DocumentType, page) => {
      const fields = page.fields as FieldType
      memo[fields.title] = {
        content: fields && fields.content,
        path: fields.path,
        tags: fields.tags
      } as ContentType
      return memo
    }, {})

    const documents = await client.getEntries({
      content_type: "document", // eslint-disable-line
      limit: 1000
    })
    serverData.documents = documents.items.reduce(
      (memo: DocumentType, document) => {
        const fields = document.fields as FieldType
        memo[fields.title] = {
          content: fields && fields.content,
          path: fields.path,
          tags: fields.tags
        } as ContentType
        return memo
      },
      {}
    )

    const response = await client.getEntries({
      // NOTE: These are posts
      content_type: POSTS, // eslint-disable-line
      limit: 1000
    })
    serverData.posts = response.items
      .map(post => {
        const fields = post.fields as HomepageField
        return {
          body: fields && fields.body,
          category: fields.category[0].fields.title,
          image: fields.featuredImage.fields.file.url,
          order: fields.order,
          secondaryBody: fields.secondaryBody,
          title: fields && fields.title
        } as HomepageContentType
      })
      .sort((a, b) => {
        if (a.order < b.order) {
          return -1
        }
        if (a.order > b.order) {
          return 1
        }
        return 0
      })

    serverData.tagNames = getTags(serverData.documents)
    serverData.documentNames = filterDocuments(serverData.documents, query)

    const blocksResult = await client.getEntries({
      content_type: "block", // eslint-disable-line
      limit: 1000
    })
    serverData.blocks = blocksResult.items.map(i => i.fields) as BlockType[]

  } catch (e) {
    // tslint:disable-next-line
    console.log("error", e)
  }

  return serverData
}

const getSiteContent = async (query: QueryType, jwt: string): Promise<any> => {
  let props: any = {}
  try {
    let events
    try {
      events = await getEvents()
      console.log('events:', events)
    } catch (e) {
      console.warn('Error fetching SPP events:', e)
    }

    // Get content from Contentful
    const content = await getBlogContent(query)
    const { pages, posts, documents, tagNames, documentNames, blocks } = content

    // FIXME This `app` object is arbitrary and meaningless
    const app = { query, content }

    const contentProps = {
      app,
      events,
      documentNames,
      documents,
      posts,
      blocks,
      tagNames,
      pages
    }
    props = { ...props, ...contentProps }

    const electionsData = await getElectionsData(jwt)
    props = { ...props, ...electionsData }

  } catch (e) {
    // tslint:disable-next-line
    console.log("caught exception", e)
  }

  return props
}

const getElectionsData = async (jwt: string): Promise<any> => {
  let ballots: Ballot[] = []
  let elections: Election[] = []
  if (jwt) {
    ballots = await getMyBallots(jwt)
    elections = await getElections()
  }

  return { ballots, elections }
}

export default getSiteContent
