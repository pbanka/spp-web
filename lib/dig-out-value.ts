/**
 * `obj` can be an object with key or not. If the key is
 * present, it will either be a string or an array of strings.
 * Pull out the right value or return an empty string.
 */
export default function digOutValue(obj: any, key: string): string {
  const value = obj[key] || ""
  if (typeof value === "string") {
    return value
  }
  return value[0]
}
