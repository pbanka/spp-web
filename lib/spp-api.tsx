import fetch from "isomorphic-fetch"
import {
  Ballot,
  BallotInput,
  Election
} from "../types/vote"
import Event from "../types/event"

const baseUrl = process.env.sppApiBaseUrl

export function sppApiHeaders(jwt?: string) {
  let headers: object = {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
  if (jwt) {
    headers = {
      ...headers, Authorization: `Bearer ${jwt}`
    }
  }
  return headers
}

export async function getMyBallots(jwt: string): Promise<Ballot[]> {
  const endpointUrl = baseUrl + "/me/ballots"
  const response = await fetchAndLog(endpointUrl, { headers: sppApiHeaders(jwt) })
  const data = await response.json()
  return data.data
}

export async function getElections(): Promise<Election[]> {
  const endpointUrl = baseUrl + "/elections"
  const response = await fetchAndLog(endpointUrl)
  const data = await response.json()
  return data.data
}

export async function postMyBallot(ballot: BallotInput, jwt: string): Promise<Ballot> {
  const endpointUrl = baseUrl + "/me/ballots"
  const response = await fetchAndLog(endpointUrl, {
    body: JSON.stringify({ data: ballot }),
    headers: sppApiHeaders(jwt),
    method: "POST"
  })
  const data = await response.json()
  if (response.status !== 201) {
    console.warn('Get response body', data)
    throw new Error(`Unexpected HTTP status: ${response.status}`)
  }
  return data.data
}

async function fetchAndLog(url: string, options?: any): Promise<any> {
  let allOpts = { ...(options || {}), url }
  let { headers } = allOpts
  if (headers && headers['Authorization']) {
    headers = { ...headers, Authorization: '[redacted]' }
    allOpts = { ...allOpts, headers }
  }
  console.log('SPP API call to', allOpts);
  return await fetch(url, options)
}

export interface GetEventsParams {
  jwt?: string
}

export async function getEvents(params?: GetEventsParams): Promise<Event[]> {
  params = params || {}
  const endpointUrl = baseUrl + "/events?public=true"
  const response = await fetchAndLog(endpointUrl, {
    headers: sppApiHeaders(params.jwt)
  })
  if (response.status !== 200) {
    const data = await response.json()
    console.warn('response body', data)
    throw new Error(`Unexpected HTTP status for response: ${response.status}`)
  }
  const data = await response.json()
  return data.data.map(parseEvent)
}

function parseEvent(e: any): Event {
  return {
    description: e.description,
    id: e.id,
    isPublic: e.isPublic,
    signupUrl: e.signupUrl,
    startAt: new Date(e.startAt),
    title: e.title
  }
}
